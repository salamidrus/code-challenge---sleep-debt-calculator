const getSleepHours = day => {
    switch(day) {
      case 'monday':
        return 6;
      break;
      case 'tuesday':
        return 5;
      break;
      case 'wednesday':
        return 5;
      break;
      case 'thursday':
        return 4;
      break;
      case 'friday':
        return 6;
      break;
      case 'saturday':
        return 5;
      break;
      case 'sunday':
        return 4;
      break;   
    }
  }
  
  const getActualSleepHours = () => 
    getSleepHours('monday') +
    getSleepHours('tuesday') +
    getSleepHours('wednesday') +
    getSleepHours('thursday') +
    getSleepHours('friday') +
    getSleepHours('saturday') +
    getSleepHours('sunday');
  
  /* to input ideal hours 
  const getIdealSleepHours = idealHours => 
    return idealHours*7;
  
  */
  const getIdealSleepHours = () => {
    const idealHours = 6;
    return idealHours*7;
  }
  
  const calculateSleepDebt = () => {
    const actualSleepHours = getActualSleepHours();
    
    const idealSleepHours = getIdealSleepHours();
  
  
  if (actualSleepHours === idealSleepHours) {
    console.log("You got the perfect amount of sleep");
  }
  
  else if (actualSleepHours > idealSleepHours) {
    console.log("You got" + (actualSleepHours - idealSleepHours) + " hour (s) more sleep than you needed this week.");
  }
  
  else if (actualSleepHours < idealSleepHours) {
    console.log("You got " + (idealSleepHours - actualSleepHours)+ " hour(s) less sleep than you needed this week. Get some rest.");
  }
  
  else {
    console.log("Error!");
  }
  }
  // coding test
  console.log(getSleepHours('monday'));
  console.log(getActualSleepHours());
  console.log(getIdealSleepHours());
  calculateSleepDebt();